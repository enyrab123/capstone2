// Dependensices and modules
const express = require("express");
const cartController = require("../controllers/cartController");
const auth = require("../auth");
const { verify, verifyAdmin } = auth;

// Routing component
const router = express.Router();

// Route for addToCart
router.post("/:userId/add-to-cart", verify, cartController.addToCart);

// Route for getUserCart
router.get("/:userId/retrieve-cart", verify, cartController.retrieveCart);

// Route for update cart quantity
router.put("/:cartId/change-quantity", verify, cartController.changeQuantity);

// Router for remove single product
router.delete(
  "/:cartId/:productId/remove-product",
  verify,
  cartController.removeProduct
);

// Router for removeItem
router.delete(
  "/:cartId/:productId/remove-products",
  verify,
  cartController.removeProductsFromCart
);

// Router for clearCart
router.delete("/:cartId/clear-cart", verify, cartController.clearCart);

module.exports = router;
