// Dependensices and modules
const express = require("express");
const orderController = require("../controllers/orderController");
const auth = require("../auth");
const { verify, verifyAdmin } = auth;

// Routing component
const router = express.Router();

// Route for createOrder
router.post("/:userId/checkout", verify, orderController.checkout);

// Route for getUserOrders
router.get("/:userId/retrieve-orders", verify, orderController.retrieveOrders);

// Route for retrieveAllOrders
router.get(
  "/all-orders",
  verify,
  verifyAdmin,
  orderController.retrieveAllOrders
);

router.get("/:userId/get-user-stats", verify, orderController.getUserStats);

// Route for setting order status to ongoing
router.put(
  "/:orderId/set-order-ongoing",
  verify,
  verifyAdmin,
  orderController.setOrderOngoing
);

// Route for setting order status to completed
router.put(
  "/:orderId/set-order-completed",
  verify,
  verifyAdmin,
  orderController.setOrderCompleted
);

router.get(
  "/:orderId/retrieve-order",
  verify,
  orderController.retrieveOneOrder
);

module.exports = router;
