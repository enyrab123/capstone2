// Dependensices and modules
const express = require("express");
const userController = require("../controllers/userController");
const auth = require("../auth");
const { verify, verifyAdmin } = auth;

// Routing component
const router = express.Router();

// Route for registerUser
router.post("/registration", userController.registration);

// Route for loginUser
router.post("/login", userController.login);

// Route for getUserDetails
router.get(
  "/retrieve-user-details",
  verify,
  userController.retrieveUserDetails
);

// Get all users
router.get("/all-users", verify, verifyAdmin, userController.getUsers);

// Update user info
router.put("/:userId/update-user-info", verify, userController.updateUserInfo);

// Route for setUserAsAdmin
router.put(
  "/:userId/set-as-admin",
  verify,
  verifyAdmin,
  userController.setAsAdmin
);

// Route for setUserAsNonAdmin
router.put(
  "/:userId/set-as-non-admin",
  verify,
  verifyAdmin,
  userController.setAsNonAdmin
);

// Route for changePassword
router.post("/change-password", verify, userController.changePassword);

module.exports = router;
