// Dependensices and modules
const express = require("express");
const productController = require("../controllers/productController");
const auth = require("../auth");
const { verify, verifyAdmin } = auth;

// Routing component
const router = express.Router();

// Route for createProduct
router.post(
  "/create-product",
  verify,
  verifyAdmin,
  productController.createProduct
);

// Route for getAllProducts
router.get(
  "/retrieve-products",
  verify,
  verifyAdmin,
  productController.retrieveProducts
);

// Route for getAllActiveProducts
router.get(
  "/retrieve-active-products",
  productController.retrieveActiveProducts
);

// Route for getSingleProduct
router.get("/:productId", productController.retrieveSingleProduct);

// Route for updateProduct
router.put(
  "/:productId/update-product",
  verify,
  verifyAdmin,
  productController.updateProductInfo
);

// Route for archiveProduct
router.put(
  "/:productId/archive",
  verify,
  verifyAdmin,
  productController.archiveProduct
);

// Route for activateProduct
router.put(
  "/:productId/activate",
  verify,
  verifyAdmin,
  productController.activateProduct
);

module.exports = router;
