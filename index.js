// Dependencies and modules
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

// Environment setup
const port = 4000;

// Server setup
const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors()); // Allows our backend application to be available to our frontend application

const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");
const orderRoutes = require("./routes/orderRoutes");
const cartRoutes = require("./routes/cartRoutes"); // For testing

// Database connection
// Connect to our MongoDB database
mongoose.connect(
  "mongodb+srv://enyrab123:mr3nyr4b01@b297.riaw3aa.mongodb.net/capstone2?retryWrites=true&w=majority",
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);

// Prompt status for our database
let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection error"));
db.once("open", () => console.log("Connected to MongoDB Atlas."));

// [Backend routes]
// http://localhost:4000/users
app.use("/users", userRoutes);
app.use("/products", productRoutes);
app.use("/orders", orderRoutes);
app.use("/cart", cartRoutes); // For testing

// Server gateway response
if (require.main === module) {
  app.listen(process.env.PORT || port, () => {
    console.log(`API is now online on port ${process.env.PORT || port}`);
  });
}

module.exports = { app, mongoose };
