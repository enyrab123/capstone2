## E-COMMERCE API DOCUMENTATION

***TEST ACCOUNTS:***
- Regular User:
     - email: enyrab@gmail.com
     - pwd: pass123
- Admin User:
    - email: admin@gmail.com
    - pwd: admin123

***

***USER ROUTES:***
- User registration (POST)
	- http://localhost:4000/users/login
    - request body: 
        - email (string)
        - password (string)
- User authentication (POST)
	- http://localhost:4000/users/login
    - request body: 
        - email (string)
        - password (string)
- Retrieve user details (GET)
	- http://localhost:4000/users/view-user-details
    - request body: none
- Set user as admin (Auth, Admin only) (PUT)
	- http://localhost:4000/:userId/set-as-admin
    - request body: none
- Set user as non admin (Auth, Admin only) (PUT)
    - http://localhost:4000/:userId/set-as-non-admin
    - request body: none
- Change user password
    - http://localhost:4000/change-password
    - request body: newPassword(string)
    
***

***PRODUCT ROUTES:***
- Create product (Auth, Admin only) (POST)
    - http://localhost:4000/product/create-product
    - request body: 
        - name (string)
        - description (string)
        - price (number)
- Retrieve all products (Auth, Admin only) (GET)
    - http://localhost:4000/product/retrieve-products
    - request body: none
- Retrieve all active products (GET)
    - http://localhost:4000/product/retrieve-active-products
    - request body: none
- Retrieve single product (GET)
    - http://localhost:4000/product/:productId
    - request body: none
- Update product info (Auth, Admin only) (PUT)
    - http://localhost:4000/product/:productId
    - request body: 
        - name (string)
        - description (string)
        - price (number)
- Archive product (PUT)
    - http://localhost:4000/:productId/archive
    - request body: none
- Activate product (PUT)
    - http://localhost:4000/:productId/activate
    - request body: none

***

***CART ROUTES:*** 

*Note:<br />
If user has an existing cart, the product will be inserted to the existing cart. If no existing cart, a new instance of cart will be created to store the products.<br />
If the cart no longer has product, the cart document will be deleted in the db.<br />
The add to cart will not execute if the product is inactive*

- Add product (Auth) (POST)
    - http://localhost:4000/cart/:userId/add-to-cart
    - request body:
        - products (array of objects)
- Change product quantity (Auth) (GET)
    - http://localhost:4000/cart/:cartId/update-quantity
    - request body:
        - products (array of objects)
            - productId (objectId)
            - quantity (number)
- Remove products from cart (Auth) (DEL)
    - http://localhost:4000/cart/:cartId/remove-product
    - request body:
        - productId (array of objects)
- Clear cart
    - http://localhost:4000/cart/:cartId/clear-cart
    - request body:none

***

***ORDER ROUTES:*** 

*Note:<br />
If user checkouts/creates an order of the cart, the cart of the user will be automatically removed from the db*

- Create order/User checkout (Auth) (POST)
    - http://localhost:4000/orders/:userId/create-order
    - request body: 
        - userId (objectId)
        - products (array of objects)
- Retrieve user's orders (Auth) (GET)
    - http://localhost:4000/orders/:userId/orders
    - request body: none
- Retrieve all orders (Auth, Admin only) (GET)
    - http://localhost:4000/orders/all-orders
    - request body: none
