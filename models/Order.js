// Dependencies
const mongoose = require("mongoose");

const { Schema } = mongoose;

const orderSchema = new mongoose.Schema({
  userId: {
    type: String,
    required: [true, "User ID is required"],
  },

  products: [
    {
      productId: {
        type: Schema.Types.ObjectId,
        required: [true, "Product ID is required"],
      },

      quantity: {
        type: Number,
        required: [true, "Quantity is required"],
      },
    },
  ],

  totalAmount: {
    type: Number,
    required: [true, "total amount is required"],
  },

  orderStatus: {
    type: String,
    default: "Ongoing",
  },

  purchasedOn: {
    type: Date,
    default: Date.now,
  },
});

module.exports = mongoose.model("Order", orderSchema);
