// Dependencies
const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
  email: {
    type: String,
    required: [true, "Email is required"],
  },

  firstName: {
    type: String,
    required: [true, "First Name is required"],
  },

  lastName: {
    type: String,
    required: [true, "Last Name is required"],
  },

  contactNo: {
    type: String,
    required: [true, "Contact Number is required"],
  },

  address: {
    type: String,
    required: [true, "Address is required"],
  },

  password: {
    type: String,
    required: [true, "Password is required"],
  },

  isAdmin: {
    type: Boolean,
    default: false,
  },

  joinedDate: {
    type: Date,
    default: Date.now,
  },
});

module.exports = mongoose.model("User", userSchema);
