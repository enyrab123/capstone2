// Dependencies
const mongoose = require("mongoose");

const { Schema } = mongoose;

const cartSchema = new mongoose.Schema({
  userId: {
    type: String,
    required: [true, "User ID is required"],
  },

  products: [
    {
      productId: {
        type: Schema.Types.ObjectId,
        required: [true, "Product ID is required"],
      },

      name: {
        type: String,
        required: [true, "Name is required"],
      },

      image: {
        type: String,
      },

      price: {
        type: Number,
      },

      quantity: {
        type: Number,
        required: [true, "Quantity is required"],
      },

      subTotal: {
        type: Number,
        required: [true, "Subtotal is required"],
      },
    },
  ],

  totalAmount: {
    type: Number,
    required: [true, "total amount is required"],
  },

  addedOn: {
    type: Date,
    default: Date.now,
  },
});

module.exports = mongoose.model("Cart", cartSchema);
