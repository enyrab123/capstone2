// Dependensices and modules
const Order = require("../models/Order");
const Cart = require("../models/Cart");

// Create an order/checkout based on the cart of the user and calculate the total amount.
/*
    Note: If a checkout is executed, the cart of the user will be automatically removed from the db
*/
module.exports.checkout = async (req, res) => {
  try {
    // Retrieve the Cart document
    const cart = await Cart.findById(req.body.cartId);

    // If cart does not exist
    if (!cart) return res.status(404).json({ error: "Cart not found" });

    // Extract products and totalAmount from Cart
    const { products, totalAmount } = cart;

    // Create a new Order document
    const newOrder = new Order({
      userId: req.params.userId,
      products: products.map((product) => ({
        productId: product.productId,
        quantity: product.quantity,
      })),
      totalAmount: totalAmount,
    });

    // Save the new Order document
    await newOrder.save();

    // Delete the Cart document in the db after getting the cart
    await Cart.deleteOne({ _id: cart._id });

    return res.status(200).json({
      success: true,
      message: "Order created successfully",
      order: newOrder,
    });
  } catch (error) {
    console.error("Error creating order:", error);
    return res.status(500).json({
      success: false,
      error: "Error creating order: " + error.message,
    });
  }
};

// Retrieve all user orders
module.exports.retrieveOrders = async (req, res) => {
  try {
    // Fetch all user orders
    const result = await Order.find({ userId: req.params.userId });

    // Check if user has no order
    if (result.length === 0) return res.send("User has no order");

    return res.send(result);
  } catch (error) {
    return res.send("Error fetching user orders: " + error.message);
  }
};

// Retrieve all orders from the database
module.exports.retrieveAllOrders = async (req, res) => {
  try {
    // Fetch all products from the database
    const result = await Order.find({});

    // Check if user has no order
    if (result.length === 0) return res.send("User has no order");

    return res.send(result);
  } catch (error) {
    return res.send("Error fetching all orders: " + error.message);
  }
};

module.exports.getUserStats = async (req, res) => {
  try {
    const userId = req.params.userId; // Get the user's ID from the request parameters

    // Use aggregation to calculate both total orders and total amount spent by the user
    const result = await Order.aggregate([
      {
        $match: { userId: userId },
      },
      {
        $group: {
          _id: null,
          totalOrders: { $sum: 1 }, // Count the number of orders
          totalAmount: { $sum: "$totalAmount" }, // Sum the totalAmount field
        },
      },
    ]);

    // If there are results, extract both totalOrders and totalAmount; otherwise, set them to 0
    const totalOrders = result.length > 0 ? result[0].totalOrders : 0;
    const totalAmount = result.length > 0 ? result[0].totalAmount : 0;

    res.json({ success: true, totalOrders, totalAmount });
  } catch (error) {
    return res.send({
      success: false,
      message: "Error fetching all orders: " + error.message,
    });
  }
};

module.exports.setOrderOngoing = async (req, res) => {
  try {
    const result = await Order.findById(req.params.orderId);

    // Check if no product found
    if (!result)
      return res.send({ success: false, message: "Order not found" });

    // Check if product is ongoing
    if (result.orderStatus == "Ongoing")
      return res.send({
        success: false,
        message: "Order is already ongoing",
      });

    // Create a instance to set orderStatus to ongoing
    const statusField = {
      orderStatus: "Ongoing",
    };

    // Find and update the product by its ID
    const setToOngoing = await Order.findByIdAndUpdate(
      req.params.orderId,
      statusField
    );

    return res.send({
      success: true,
      message: "You have set status to Ongoing",
    });
  } catch (error) {
    return res.send({
      message: "Error on setting order status" + error.message,
    });
  }
};

module.exports.setOrderCompleted = async (req, res) => {
  try {
    const result = await Order.findById(req.params.orderId);

    // Check if no product found
    if (!result)
      return res.send({ success: false, message: "Order not found" });

    // Check if product is ongoing
    if (result.orderStatus == "Completed")
      return res.send({
        success: false,
        message: "Order is already Completed",
      });

    // Create a instance to set orderStatus to ongoing
    const statusField = {
      orderStatus: "Completed",
    };

    // Find and update the product by its ID
    const setToCompleted = await Order.findByIdAndUpdate(
      req.params.orderId,
      statusField
    );

    return res.send({
      success: true,
      message: "You have set status to Completed",
    });
  } catch (error) {
    return res.send({
      message: "Error on setting order status" + error.message,
    });
  }
};

// Retrieve one order
module.exports.retrieveOneOrder = async (req, res) => {
  try {
    // Fetch all user orders
    const result = await Order.findById(req.params.orderId);

    // Check if no order
    if (!result) return res.send("Order not found");

    return res.send(result);
  } catch (error) {
    return res.send("Error fetching order: " + error.message);
  }
};
