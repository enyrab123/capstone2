// Dependensices and modules
const Cart = require("../models/Cart");
const Product = require("../models/Product");

// Add products to cart and calculate their subtotal and total amount
/*
	Note: 
	- If there is an existing cart for the user, the product will be inserted to the existing cart, If no existing cart, a new instance of cart will be created to store the products
	- don't allow product to be added to cart if product is inactive
*/
module.exports.addToCart = async (req, res) => {
  try {
    // Retrieve the inputs
    const userId = req.params.userId;
    const productsToAdd = req.body.products;

    console.log(userId);

    // Check if a cart already exists for the user
    let existingCart = await Cart.findOne({ userId });

    // If no existing cart, creates a new instance of cart
    if (!existingCart) {
      existingCart = new Cart({
        userId: userId,
        products: [],
        totalAmount: 0,
      });
    }

    // Fetch product details and calculate subtotals
    for (const product of productsToAdd) {
      const fetchedProduct = await Product.findById(product.productId);

      // Check if product is not active
      if (fetchedProduct.isActive === false) {
        return res.send(
          "Cannot add to cart due to " + fetchedProduct.name + " is not active"
        );
      }

      if (fetchedProduct) {
        product.name = fetchedProduct.name;
        product.price = fetchedProduct.price;
        product.image = fetchedProduct.image;
        product.subTotal = fetchedProduct.price * product.quantity;
      }
    }

    // Add products to the cart and update totalAmount
    for (const product of productsToAdd) {
      const existingProduct = existingCart.products.find((p) =>
        p.productId.equals(product.productId)
      );
      if (existingProduct) {
        existingProduct.quantity += product.quantity;
        existingProduct.subTotal += product.subTotal;
      } else {
        existingCart.products.push(product);
      }
    }

    // Update the total amount of the cart
    existingCart.totalAmount = existingCart.products.reduce(
      (total, product) => total + product.subTotal,
      0
    );

    // Save the cart to the database
    const savedCart = await existingCart.save();

    // Respond with a success message and the saved cart data
    return res.status(201).json({
      success: true,
      message: "Item added to cart successfully",
      cart: savedCart,
    });
  } catch (error) {
    // Handle errors by responding with an error message
    return res
      .status(500)
      .json({ success: false, error: "Error adding cart: " + error.message });
  }
};

// Retrieve cart of user
module.exports.retrieveCart = async (req, res) => {
  try {
    // Fetch all user orders
    const result = await Cart.find({ userId: req.params.userId });

    // Check if user has no order
    if (result.length === 0) return res.send({ message: "User has no cart" });

    return res.send(result);
  } catch (error) {
    return res.send({
      message: "Error fetching user orders: " + error.message,
    });
  }
};

// Update products in the cart and recalculate subTotal and totalAmount
module.exports.changeQuantity = async (req, res) => {
  try {
    const cartId = req.params.cartId;
    const updatedProducts = req.body.products;
    let totalAmount = 0;

    // Find the cart by its ID
    const existingCart = await Cart.findById(cartId);

    // If no cart found
    if (!existingCart) return res.status(404).json({ error: "Cart not found" });

    // Update quantities and recalculate subtotals
    for (const updatedProduct of updatedProducts) {
      const existingProductIndex = existingCart.products.findIndex(
        (item) => item.productId.toString() === updatedProduct.productId
      );

      if (existingProductIndex !== -1) {
        const fetchedProduct = await Product.findById(updatedProduct.productId);
        if (fetchedProduct) {
          existingCart.products[existingProductIndex].quantity =
            updatedProduct.quantity;
          existingCart.products[existingProductIndex].subTotal =
            fetchedProduct.price * updatedProduct.quantity;
        }
      }
    }

    // Recalculate totalAmount
    existingCart.totalAmount = existingCart.products.reduce(
      (total, item) => total + item.subTotal,
      0
    );

    // Save the updated cart
    const updatedCart = await existingCart.save();

    return res
      .status(200)
      .json({ message: "Cart updated successfully", cart: updatedCart });
  } catch (error) {
    return res
      .status(500)
      .json({ error: "Error updating cart: " + error.message });
  }
};

module.exports.removeProduct = async (req, res) => {
  try {
    const cartId = req.params.cartId;
    const productId = req.params.productId;

    // Find the cart by ID
    const cart = await Cart.findById(cartId);

    if (!cart) {
      return res
        .status(404)
        .json({ success: false, message: "Cart not found" });
    }

    // Find the index of the product with the specified _id
    const productIndex = cart.products.findIndex(
      (product) => product.productId.toString() === productId
    );

    if (productIndex === -1) {
      return res
        .status(404)
        .json({ success: false, message: "Product not found in the cart" });
    }

    // Calculate the new total amount (if needed)
    cart.totalAmount -= cart.products[productIndex].subTotal;

    // Remove the product from the products array
    cart.products.splice(productIndex, 1);

    // Save the updated cart
    await cart.save();

    if (!cart.products.length) {
      await Cart.findByIdAndDelete(cart.id);
      return res.json({
        success: true,
        message: "Card is empty",
      });
    } else {
      return res.status(200).json({
        success: true,
        message: "Product removed from cart",
        updatedCart: cart,
      });
    }
  } catch (error) {
    console.error(error);
    return res.status(500).json({ message: "Internal server error" });
  }
};

// Removes a product in the cart. If there is no longer product in the cart remove the cart document in the db
module.exports.removeProductsFromCart = async (req, res) => {
  try {
    const cartId = req.params.cartId;
    const productIdsToRemove = req.body.productIds;

    // Find the cart by its ID
    const cart = await Cart.findById(cartId);

    let removedTotalAmount = 0;

    // Iterate through productIdsToRemove and remove matching products from cart
    for (const productId of productIdsToRemove) {
      const cartProductIndex = cart.products.findIndex(
        (product) => product.productId.toString() === productId
      );

      if (cartProductIndex !== -1) {
        const removedProduct = cart.products.splice(cartProductIndex, 1)[0];
        removedTotalAmount += removedProduct.subTotal;
      }
    }

    // Update cart's totalAmount
    cart.totalAmount -= removedTotalAmount;

    // If there are no products left in the cart, delete the document
    if (cart.products.length === 0) {
      await Cart.findByIdAndDelete(cartId);
      return res.status(200).json({ message: "Cart emptied and removed" });
    }

    // If there is a product left in the cart, save changes to the cart
    await cart.save();

    // Return updated cart and success message
    return res.status(200).json({
      message: "Selected product/s removed from cart",
      updatedCart: cart,
    });
  } catch (error) {
    // Handle errors during the process
    return res
      .status(500)
      .json({ error: "Error removing products from cart: " + error.message });
  }
};

// Clear all products in the cart
module.exports.clearCart = async (req, res) => {
  try {
    const cartId = req.params.cartId;

    const cart = await Cart.findById(cartId);

    // If no cart found
    if (!cart) return ressend({ success: false, message: "Cart not found" });

    cart.products = []; // Clear all products from the cart
    cart.totalAmount = 0; // Reset the total amount

    await cart.save();

    return res.send({
      success: true,
      message: "Cart cleared successfully",
      cart: cart,
    });
  } catch (error) {
    return res
      .status(500)
      .json({
        success: false,
        message: "Error clearing cart: " + error.message,
      });
  }
};
