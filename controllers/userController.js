// Dependensices and modules
const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");

// Create a new unique user and save it to the database
module.exports.registration = async (req, res) => {
  try {
    const result = await User.findOne({ email: req.body.email });

    // Check if email already exist
    if (result) return res.send({ message: "Email already exist. Try again." });

    // Create a new user instance with hashed password
    const newUser = new User({
      email: req.body.email,
      firstName: req.body.firstName,
      lastName: req.body.lastName,
      contactNo: req.body.contactNo,
      address: req.body.address,
      password: bcrypt.hashSync(req.body.password, 10),
    });

    // Save the new user to the database
    const saveUser = await newUser.save();

    return res.send(true);
  } catch (error) {
    // Handles error
    return res.send({ message: error.message });
  }
};

// User login
module.exports.login = async (req, res) => {
  try {
    // Find user by email
    const result = await User.findOne({ email: req.body.email });

    if (!result) {
      // If user not found
      return res.send({ message: "User not found." });
    } else {
      // Check if password is correct
      const isPasswordCorrect = bcrypt.compareSync(
        req.body.password,
        result.password
      );

      if (isPasswordCorrect) {
        // Create and send access token
        return res.send({
          id: result._id,
          access: auth.createAccessToken(result),
        });
      } else {
        return res.send({ message: "Incorrect password. Try again." });
      }
    }
  } catch (error) {
    return res.send({ message: error.message });
  }
};

// Get all users
module.exports.getUsers = async (req, res) => {
  try {
    const result = await User.find({});

    return res.send(result);
  } catch (error) {
    return res.send("Error fetching products: " + error.message);
  }
};

// Update user info (Admin only)
module.exports.updateUserInfo = async (req, res) => {
  try {
    //Find a product by its ID
    const result = await User.findById(req.params.userId);

    // Check if no user found
    if (!result) return res.send({ success: false, message: "No user found." });

    // Find and update the product by its ID
    const updatedUser = {
      email: req.body.email,
      firstName: req.body.firstName,
      lastName: req.body.lastName,
      contactNo: req.body.contactNo,
      address: req.body.address,
    };

    const updateUser = await User.findByIdAndUpdate(
      req.params.userId,
      updatedUser
    );

    return res.send({ success: true, message: "Successfully updated" });
  } catch (error) {
    return res.send({
      success: false,
      message: "Error updating the user: " + error.message,
    });
  }
};

// Retrieve user details from the database based on the provided user ID
module.exports.retrieveUserDetails = async (req, res) => {
  try {
    // Assuming you have the User model imported, you can access the user's ID from req.user
    const userId = req.user.id;

    // Use the user's ID to retrieve user details from the database
    const result = await User.findById(userId);

    // If no user found
    if (!result) return res.send({ message: "User not found." });

    return res.send(result);
  } catch (error) {
    return res.send({ message: error.message });
  }
};

// Set user as admin
module.exports.setAsAdmin = async (req, res) => {
  try {
    const result = await User.findById(req.params.userId);

    // Check if no user found
    if (!result) return res.send({ success: false, message: "User not found" });

    // Check if user is an admin
    if (result.isAdmin == true)
      return res.send({ success: false, message: "User is already an admin" });

    // Create a instance to set isAdmin to true
    const adminField = {
      isAdmin: true,
    };

    // Find and update the user by its ID
    const setToAdmin = await User.findByIdAndUpdate(
      req.params.userId,
      adminField
    );

    return res.send({
      success: true,
      message: "You have set " + setToAdmin.email + " as admin",
    });
  } catch (error) {
    return res.send({
      message: "Error on setting user as admin." + error.message,
    });
  }
};

// Set user as non-admin
module.exports.setAsNonAdmin = async (req, res) => {
  try {
    const result = await User.findById(req.params.userId);

    // Check if no user found
    if (!result) return res.send({ success: false, message: "User not found" });

    // Check if user is a non-admin
    if (result.isAdmin == false)
      return res.send({
        success: false,
        message: "User is already a non-admin",
      });

    // Create a instance to set isAdmin to true
    const adminField = {
      isAdmin: false,
    };

    // Find and update the user by its ID
    const setToNonAdmin = await User.findByIdAndUpdate(
      req.params.userId,
      adminField
    );

    return res.send({
      success: true,
      message: "You have set " + setToNonAdmin.email + " as non-admin",
    });
  } catch (error) {
    return res.send({
      message: "Error on setting user as admin." + error.message,
    });
  }
};

// Change passord
module.exports.changePassword = async (req, res) => {
  try {
    // Get new password and id of user using destructuring
    const { newPassword } = req.body;
    const { id } = req.user; // Extracting user ID from the authorization header

    // Hashing the new password
    const hashedPassword = await bcrypt.hash(newPassword, 10);

    // Updating user's password in the database
    await User.findByIdAndUpdate(id, { password: hashedPassword });

    // Sending a success response
    res.status(200).json({ message: "Password reset successfully" });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Internal server error" });
  }
};
