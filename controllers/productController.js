// Dependencies and modules
const Product = require("../models/Product");

// Create a new product and save it to the database
module.exports.createProduct = async (req, res) => {
  try {
    const name = req.body.name;

    // Check if product already exist
    let isExist = await Product.findOne({ name });

    if (isExist)
      return res.send({ success: false, message: "Product already exist" });

    // Create a new product instance
    const newProduct = new Product({
      name: req.body.name,
      description: req.body.description,
      price: req.body.price,
      image: req.body.image,
    });
    // Save the new product to the database
    const saveProduct = await newProduct.save();

    return res.send({
      success: true,
      message: "New product created.",
      product: saveProduct,
    });
  } catch (error) {
    return res.send({
      success: false,
      message: "Error on creating the product: " + error.message,
    });
  }
};

// Retrieve all products from the database
module.exports.retrieveProducts = async (req, res) => {
  try {
    // Fetch all products from the database
    const result = await Product.find({});

    return res.send(result);
  } catch (error) {
    return res.send("Error fetching products: " + error.message);
  }
};

// Retrieve all active products from the database where "isActive" is true
module.exports.retrieveActiveProducts = async (req, res) => {
  try {
    // Fetch all product where isActive is true
    const result = await Product.find({ isActive: true });

    // Check if no active products
    if (result.length === 0) return res.send("No active products");

    return res.send(result);
  } catch (error) {
    return res.send("Error fetching products: " + error.message);
  }
};

// Retrieve a single product from the database by its ID
module.exports.retrieveSingleProduct = async (req, res) => {
  try {
    // Find a product by its ID
    const result = await Product.findById(req.params.productId);

    // Check if no product found
    if (!result) return res.send("Product not found");

    return res.send(result);
  } catch (error) {
    return res.send("Error fetching single product: " + error.message);
  }
};

// Update product details based on the provided request
module.exports.updateProductInfo = async (req, res) => {
  try {
    // Find a product by its ID
    const result = await Product.findById(req.params.productId);

    // Check if no product found
    if (!result) return res.send(false);

    // Create an object with updated product details
    const updatedProduct = {
      name: req.body.name,
      description: req.body.description,
      price: req.body.price,
    };
    // Find and update the product by its ID
    const updateProduct = await Product.findByIdAndUpdate(
      req.params.productId,
      updatedProduct
    );

    return res.send(true);
  } catch (error) {
    return res.send("Error updating the product: " + error.message);
  }
};

// Archive a product by setting isActive to false
module.exports.archiveProduct = async (req, res) => {
  try {
    // Find a product by its ID
    const result = await Product.findById(req.params.productId);

    // Check if no product found
    if (!result)
      return res.send({ success: false, message: "Product not found" });

    // Create an instance for archiving the product
    const archiveField = {
      isActive: false,
    };

    // Find and update the product by its ID
    const archiveProduct = await Product.findByIdAndUpdate(
      req.params.productId,
      archiveField
    );

    return res.send({
      success: true,
      message: "You have archived " + archiveProduct.name,
    });
  } catch (error) {
    return res.send({
      success: false,
      message: "Error archiving the product: " + error.message,
    });
  }
};

// Activate a product by setting isActive to false
module.exports.activateProduct = async (req, res) => {
  try {
    // Find a product by its ID
    const result = await Product.findById(req.params.productId);

    // Check if no product found
    if (!result)
      return res.send({ success: false, message: "Product not found" });

    // Create an instance for activating the product
    const activateField = {
      isActive: true,
    };
    // Find and update the product by its ID
    const activateProduct = await Product.findByIdAndUpdate(
      req.params.productId,
      activateField
    );

    return res.send({
      success: true,
      message: "You have activated " + activateProduct.name,
    });
  } catch (error) {
    return res.send({
      success: false,
      message: "Error activating the product: " + error.message,
    });
  }
};
